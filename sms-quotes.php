<?php
/*
Plugin Name: SMS Quotes
Plugin URI: http://www.roadsidemultimedia.com
Description: Add quotes to a site
Author: Roadside Multimedia
Contributors: Milo Jennings
PageLines: true
Version: 1.0
Section: true
Class Name: SMS_Quote
Filter: component
Loading: active
Bitbucket Plugin URI: https://bitbucket.org/roadsidemultimedia/sms-quote/
Bitbucket Branch: master
*/

add_action('plugins_loaded', function(){

	include_once ( dirname( __FILE__ ) . "/admin/cpt-init.php" );
	include_once ( dirname( __FILE__ ) . "/admin/cpt-fields.php" );
	include_once ( dirname( __FILE__ ) . "/admin/global-fields.php" );

}, 20);

/**
 * IMPORTANT
 * This tells wordpress to not load the class as DMS will do it later when the main sections API is available.
 * If you want to include PHP earlier like a normal plugin just add it above here.
 */

if( ! class_exists( 'PageLinesSectionFactory' ) )
	return;

class SMS_Quote extends PageLinesSection {

	function section_head() {
		add_action( 'pl_scripts_on_ready', array( $this, 'script' ) );
	}

	function script() {

		/*// Remove padding that Pagelines adds to all sections
		$clone = $this->meta['clone'];
		ob_start();
		?>jQuery( '.section-sms-quotes div' ).removeClass('pl-section-pad fix');
		<?php
		return ob_get_contents();*/
		
	}

	function section_opts(){


			global $sms_utils;
			$sms_options = get_option('sms_options');

			// $size_name_list = $sms_utils->convert_redux_choices_to_dms( $sms_options['fonts']['size-name-list'] );
			$weight_name_choices = $sms_utils->convert_redux_choices_to_dms( $sms_options['fonts']['weight-name-list'] );
			$text_align_choices = $sms_utils->convert_redux_choices_to_dms( $sms_options['fonts']['text-alignment-list'] );
			$line_height_choices = $sms_utils->convert_redux_choices_to_dms( $sms_options['fonts']['line-height-list'] );

			$text_transform_list_array = $sms_utils->filter_out_redundant_css_properties( $sms_options['fonts']['text-transform-list'] );
			$text_transform_choices = $sms_utils->convert_redux_choices_to_dms( $text_transform_list_array );

			$style_type_choices = $sms_utils->convert_redux_choices_to_dms( $sms_options['fonts']['heading-type-list'] );

			$variant_key = 'quote';
			$variant_title = 'Quote';

			$field_array = array(
				array(
					'type'          => 'check',
					'key'           => 'enable_indicators',
					'title'         => 'Enable Visual Indicators?',
					'scope'         => 'global',
					'default'       => false,
				),
				array(
					'type'          => 'textarea',
					'title'         => "{$variant_title} Text",
					'key'           => 'text',
				),
				array(
					'type'          => 'check',
					'key'           => 'cite_enable',
					'title'         => 'Enable citation?',
					'default'       => false
				),
				array(
					'type'          => 'text',
					'title'         => 'Citation Text',
					'key'           => 'citation_text',
				),
				array(
					'type'          => 'select',
					'title'         => 'Select a global style',
					'key'           => 'type',
					'opts'=> array(
						'primary'   => array( 'name' => 'Primary' ),
						'secondary' => array( 'name' => 'Secondary' ),
						'tertiary'  => array( 'name' => 'Tertiary' ),
					),
				),
				array(
					'type'          => 'select',
					'title'         => 'Alignment (Override)',
					'key'           => 'align',
					'opts'          => $text_align_choices,
				),
				array(
					'type'          => 'select',
					'title'         => 'Text Transform (Override)',
					'key'           => 'text_transform',
					'opts'          => $text_transform_choices,
				),
				array(
					'type'          => 'text',
					'title'         => 'Link URL',
					'key'           => 'link_url',
				),
				array(
					'type'          => 'check',
					'key'           => 'link_target',
					'title'         => 'Open link in new window?',
					'default'       => false
				),
			);

			$options = array();

			// Generate fields for DMS
			$temp = array();
			foreach ($field_array as $field) {
				// echo $variant_key;
				$temp[] = array(
					'title' => $field['title'],
					'key'   => "{$variant_key}_{$field['key']}",
					'type'  => $field['type'],
					'opts'  => $field['opts'],
					'scope'	=> $field['scope'],
				);
			}

			$options[] = array(
				'title' => 'Quote',
				'type'  => 'multi',
				'opts'  => $temp
			);
			// echo "<pre>\$options: " . print_r($options, true) . "</pre>";

			return $options;
		}
		function section_template(){

			$sms_options = get_option('sms_options');

			$quote_type            = ($this->opt('quote_type')) ? $this->opt('quote_type') : 'primary';
			$quote_text            = ($this->opt('quote_text')) ? $this->opt('quote_text') : 'Default quote text';
			$quote_cite_text       = ($this->opt('quote_cite_text')) ? $this->opt('quote_cite_text') : 'Default citation text';
			$quote_align           = ($this->opt('quote_align')) ? ' align-'.$this->opt('quote_align') : '';
			$quote_text_transform  = ($this->opt('quote_text_transform')) ? ' text-'.$this->opt('quote_text_transform') : '';
			$quote_link_url        = ($this->opt('quote_link_url')) ? $this->opt('quote_link_url') : '';
			$quote_link_target     = ($this->opt('quote_target')) ? ' target="' . $this->opt('quote_target') . '"' : '';

			$quote_classes = "{$quote_align}{$quote_text_transform}";


			$indicator_output = '';
			// Only show indicators to administrators
			if ( current_user_can('administrator') && pl_setting('quote_enable_indicators') ){
				
				$indicator_output .= "<div class='row'>";

				$indicator_output .= "<div class='sms-indicator sms-indicator-light'>Quote Properties</div>";

				if( $quote_type )
					$indicator_output .= "<div class='sms-indicator'><i class='fa fa-tachometer'></i><span><strong>". ucfirst($quote_type) ."</strong></span></div>";
				if( $this->opt('quote_align') ){
					$indicator_output .= "<div class='sms-indicator'><i class='fa fa-align-{$this->opt('quote_align')}'></i><span>alignment: <strong>{$this->opt('quote_align')}</strong></span></div>";
				}
				if( $this->opt('quote_text_transform') )
					$indicator_output .= "<div class='sms-indicator'><i class='fa fa-text-height'></i><span>{$this->opt('quote_text_transform')}</span></div>";

				$indicator_output .= "</div>";

			}

			// If anything was added to indicator output var, wrap it all in a div
			if($indicator_output){
				$indicator_output = "<div class='sms-indicator-wrap'>".$indicator_output."</div>";

				$indicator_css = "

					.sms-indicator-wrap{
						visibility: hidden;
						display: none;
						position: absolute;
						top: auto;
						bottom: 100%;
						margin-bottom: 2px;
						width: 100%;
						text-align: center;
						font-family: 'Gill Sans', 'Gill Sans MT', Calibri, sans-serif;
						background: rgba(0,0,0,0.5);
					}
					.drag-drop-editing .section-sms-quotes:hover .sms-indicator-wrap{
						visibility: visible;
						display: block;
						z-index: 101;
					}

					.sms-indicator-wrap-title{
						font-size: 12px;
						margin-right: 10px;
						text-transform: uppercase;
						color: #fff;
					}


					.sms-indicator {
						font-size: 12px;
						display: inline-block;
						margin-right: 5px;
						margin-top: 2px;
						margin-bottom: 2px;
						background: rgba(0,0,0,.2);
						border: solid 1px rgba(255,255,255,.4);
						color: #fff;
						padding-left: 8px;
						padding-right: 8px;
						border-radius: 10px;
					}
					.sms-indicator span{
						margin-left: 5px;
					}
					.sms-indicator-light {
						background: none;
						border: none;
						float: left;
					}
					";
				$indicator_output .= inline_css_markup('sms-quote-indicator-css', $indicator_css, false);
			}


		/* 
		/* markup structure taken from here: http://html5doctor.com/cite-and-blockquote-reloaded/
		 * <blockquote>
		 *   <p>content</p>
		 *   <footer>
		 *     <cite>citation</cite>
		 *   </footer>
		 * </blockquote>
		 */
		if( $this->opt('quote_cite_enable') ){
			$output_cite = sprintf('<cite class="sms-cite">%1$s</cite>', $quote_cite_text);
		} else {
			$output_cite = '';
		}

		$output_quote = sprintf('<blockquote class="sms-quote sms-quote--%1$s%3$s"><p>%2$s</p>%4$s</blockquote>', $quote_type, $quote_text, $quote_classes, $output_cite);
		if( $quote_link_url ){
			// Wrap in an A tag
			$output_quote = sprintf('<a href="%1s"%2s class="sms-quote-link">%3s</a>', $quote_link_url, $quote_target, $output_quote);
		}

		if($output_quote){
			$output_quote .= $indicator_output;
		}

		// echo $output;
		echo $output_quote;
	}
}
