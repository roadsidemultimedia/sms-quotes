<?php
add_action( 'init', 
  function(){
    $args = array(
      'labels' => array(
        'name' => __( 'Quote Styles' ),
        'singular_name' => __( 'Quote Style' )
      ),
      'hierarchical' => false,
      'supports' => array( 'title' ),
      'public' => false,
      'show_ui' => true,
      'show_in_menu'  => 'dms-sms.php',
      'menu_position' => 10,
      'show_in_nav_menus' => false,
      'publicly_queryable' => false,
      'exclude_from_search' => true,
      'has_archive' => false,
      'query_var' => true,
      'can_export' => true,
      'rewrite' => false,
      'capability_type' => 'page',
      'menu_icon' => 'dashicons-format-quote',
    );

    register_post_type( 'sms_quote', $args );
  }, 99
);
