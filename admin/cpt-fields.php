<?php
// class get_quote_data{

//   public $sms_redux;
//   public $sms_options;
//   public $global_selected_font_kit_id;
//   public $global_selected_font_kit_meta;

//   public function __construct(){
//     global $sms_utils;
//     global $sms_redux;
//     if (!isset($GLOBALS['sms_redux'])) {
//       $GLOBALS['sms_redux'] = get_option('sms_redux', array());
//     }

//     $this->sms_redux = $sms_redux;
//     // $this->sms_utils = $sms_utils;
//     $this->sms_options = get_option('sms_options');
//     $this->global_selected_font_kit_id = $this->sms_redux['font-kit'];
//     $this->global_selected_font_kit_meta = get_post_meta( $this->global_selected_font_kit_id , 'sms_redux', true);

//     echo "firing quote __construct";

//   }

// }

// add_action('init', function(){
  // $get_quote_data = new get_quote_data();

  // add_action('redux/metaboxes/sms_redux/boxes', 'sms_add_quote_metaboxes', 10);

  // function sms_add_quote_metaboxes($metaboxes) {

  //   global $sms_utils;
  //   global $sms_redux;
  //   // if (!isset($GLOBALS['sms_redux'])) {
  //   //   $GLOBALS['sms_redux'] = get_option('sms_redux', array());
  //   // }

  //   $sms_options = get_option('sms_options');
  //   $global_selected_font_kit_id = $sms_redux['font-kit'];
  //   $global_selected_font_kit_meta = get_post_meta( $global_selected_font_kit_id , 'sms_redux', true);

  //   echo "<pre>firing quote metabox func:</pre>";
  //   die("firing quote metabox func:");

  //   $fields = array();
  //   //=========================================
  //   // Custom Markup
  //   //-----------------------------------------

  //   //Markup
  //   $fields[] = array(
  //     'id'        => 'quote-classes',
  //     'type'      => 'text',
  //     'title'     => 'Custom Classes',
  //     'subtitle'  => 'Add quote classes here. Do not add dots.',
  //     'desc'      => 'This will also append the class of the type of quote (sms-quote-primary, sms-quote-secondary, etc...)',
  //   );

  //   //Markup
  //   $fields[] = array(
  //     'id'        => 'quote-markup',
  //     'type'      => 'ace_editor',
  //     'title'     => 'Custom Markup',
  //     'mode'      => 'html',
  //     'theme'     => 'monokai',
  //     'subtitle'  => 'Add quote markup here.',
  //     'desc'      => 'Available template placeholders {{content}}',
  //   );

  //   //CSS
  //   $fields[] = array(
  //     'id'        => 'quote-css',
  //     'type'      => 'ace_editor',
  //     'title'     => 'Custom Markup',
  //     'mode'      => 'css',
  //     'theme'     => 'monokai',
  //     'subtitle'  => 'Add quote css here.',
  //     'desc'      => 'Available template placeholders {{content}}',
  //   );

  //   // Create a Meta Box and reset the meta box array
  //   //-----------------------------------------
  //   $boxSections[]['fields'] = $fields;
  //   $metaboxes[] = array(
  //      'id' => 'quote-assets',
  //      'title' => 'Quote Assets',
  //      'post_types' => array('sms_quote'),
  //      'sections' => $boxSections
  //   );

  //   $fields = array();
  //   $boxSections = array();
  //   //=========================================
  //   /////////////////////////////////////////////////
  //   /////////////////////////////////////////////////
  //   /////////////////////////////////////////////////
  //   /////////////////////////////////////////////////

  //   $fields[] = array(
  //       'id'    => 'font-kit-assignments-tip',
  //       'type'  => 'info',
  //       'title' => 'Tip',
  //       'icon'  => 'el-icon-info-sign',
  //       'desc'  => 'In this section, you\'ll set which types of fonts are assigned to various elements throughout the site.<br>This is where all the real magic happens.',
  //   );

  //   foreach ($font_assignment_field_array as $key => $value) {

  //     $fields[] = array(
  //       'id' => "$key-start",
  //       'title' => $value['title'],
  //       'type' => 'section',
  //       'indent' => true,
  //     );

  //     //////////////////////////////////////////////////////////////////////////////
  //     //////////////////////////////////////////////////////////////////////////////

  //     $title               = $value['title'];
  //     $font_type_slug      = $this->global_selected_font_kit_meta[ $key .'-font'];

  //     // echo "<pre>\$current_font_kit_meta: " . print_r($current_font_kit_meta, true) . "</pre>";

  //     $font_type_options   = $sms_utils->get_font_type_list_as_redux_select_array($current_font_kit_id);
  //     $font_type_default   = $font_type_options[0];

  //     $font_size_options   = $sms_options['fonts']['size-name-list'];
  //     $font_size_default   = $value['default_size'];

  //     if($font_type_slug){
  //       $font_weight_options_named = $sms_utils->get_weights_for_font_type( $font_type_slug, $current_font_kit_id );

  //       // echo "<pre>\$font_type_slug: " . print_r($font_type_slug, true) . "</pre>";
  //       // echo "<pre>\$font_weight_options_named: " . print_r($font_weight_options_named, true) . "</pre>";
  //       $font_weight_default = $font_weight_options_named[0];
  //     }

  //     // Font selection
  //     $fields[] = array(
  //       'id'        => $key.'-font',
  //       'title'     => "Font Type",
  //       'type'      => 'select',
  //       'subtitle'  => 'Select Font Type (<strong>HINT:</strong>you may have to refresh twice for this to populate correctly.)',
  //       'subtitle'  => '<strong>Selected Font Family:</strong> <small>' . $this->global_selected_font_kit_meta[$font_type_slug.'-font-family'].'</small>',
  //       'default'   => $font_type_default,
  //       'options'   => $font_type_options,
  //     );

  //     // Size selection
  //     $fields[] = array(
  //       'id'        => $key.'-size',
  //       'title'     => "Size",
  //       'type'      => 'select',
  //       'default'   => $font_size_default,
  //       'options'   => $font_size_options,
  //     );

  //     // Weight selection
  //     $fields[] = array(
  //       'id'        => $key.'-weight',
  //       'title'     => "Weight",
  //       'type'      => 'select',
  //       'default'   => $font_weight_default,
  //       'options'   => $font_weight_options_named,
  //     );

  //     //////////////////////////////////////////////////////////////////////////////
  //     //////////////////////////////////////////////////////////////////////////////

  //     $fields[] = array(
  //       'id' => "$key-end",
  //       'type' => 'section',
  //       'indent' => false,
  //     );

  //   } // endforloop

  //   // Create a Meta Box and reset the meta box array
  //   //-----------------------------------------
  //   $boxSections[]['fields'] = $fields;
  //   $metaboxes[] = array(
  //      'id' => 'quote-assignments',
  //      'title' => 'Quote Assignments',
  //      'post_types' => array('sms_quote'),
  //      'sections' => $boxSections
  //   );
  //   $fields = array();
  //   $boxSections = array();

  //   // Update database with any changes to sms_options
  //   update_option('sms_options', $sms_options);

  //   return $metaboxes;
  // }
  // // $quote_fields->add_redux_metaboxes();
  // // add_action('redux/metaboxes/sms_redux/boxes', array('add_redux_metaboxes', 'add_redux_metaboxes'), 10);
  // // add_action('redux/metaboxes/sms_redux/boxes', array($quote_fields, 'add_redux_metaboxes'), 10);
  

// });

