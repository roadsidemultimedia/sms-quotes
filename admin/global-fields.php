<?php
add_filter('redux/options/sms_redux/sections', function($sections){

  // class smsGlobalQuoteOptions(){

    $sms_options = get_option('sms_options');
    global $sms_redux;
    global $sms_utils;

    $post_choices = $sms_utils->get_post_list_by_term_as_redux_field_array('sms_quote');

    $args = array(

      'title'    => 'Quote',
      'id'       => 'quote',
      'type'     => 'select',
      'slots'    => 1,
      'variants' => array(

        array(
          'id'      => 'primary',
          'title'   => 'Primary',
          'options' => $post_choices,
          'desc'    => array('link_to_post_edit' => true),
        ),
        array(
          'id'      => 'secondary',
          'title'   => 'Secondary',
          'options' => $post_choices,
          'desc'    => array('link_to_post_edit' => true),
        ),
        array(
          'id'      => 'tertiary',
          'title'   => 'Tertiary',
          'options' => $post_choices,
          'desc'    => array('link_to_post_edit' => true),
        ),

      ),
      
    );

    function get_quote_slot_variations($args){
      global $sms_redux;

      $result = array();
      foreach ($args['variants'] as $variant) {
        $i=1;

        $result[$variant['id']]['info'] = array(
          'id'    => $variant['id'],
          'title' => $variant['title'],
        );

        if($variant['desc']['link_to_post_edit'] == true){
          $slug = $args['id']."-slot-".$variant['id'];
          $post_id = $sms_redux[ $slug ];
          if($post_id){
            $post_edit_url = "/wp-admin/post.php?post=".$post_id."&action=edit";
            $description = "<a target='_blank' href='".$post_edit_url."'>Edit <strong>" . get_the_title($post_id) . "</strong> (".$variant['title'].")</a>";
          } else {
            $description = '';
          }
        } else {
          $description = '';
        }

        $result[ $variant['id'] ]['fields'][] = array(
          'id'      => $args['id']."-slot-".$variant['id'],
          'title'   => $args['title'] . " (".$variant['title'].")",
          'type'    => $args['type'],
          'select2' => array( 'allowClear'=>false ),
          'options' => $variant['options'],
          'desc'    => $description,
        );
       
      }
      return $result;
    }

    $field_variation_array = get_quote_slot_variations($args);

    $fields[] = array(
      'id' => 'quote-section-start',
      'title' => 'Global Quote Styles',
      'type' => 'section',
      'indent' => true,
    );

    // echo "<pre>\$field_variation_array: " . print_r($field_variation_array, true) . "</pre>";

    foreach ($field_variation_array['primary']['fields'] as $field) {
      $fields[] = $field;
    }
    foreach ($field_variation_array['secondary']['fields'] as $field) {
      $fields[] = $field;
    }
    foreach ($field_variation_array['tertiary']['fields'] as $field) {
      $fields[] = $field;
    }

    $fields[] = array(
      'id' => 'quote-section-section-end',
      'type' => 'section',
      'indent' => false,
    );

    $sections[] = array(
      'title' => 'Quotes',
      'desc' => '<p class="description">Assign Quote styles for use throughout the site.</p>',
      'icon' => 'el-icon-quotes',
      'fields' => $fields,
    );

    return $sections;
  // }
});
